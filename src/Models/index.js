const {sequelize} = require('../settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define('curso', {
    name: {type: DataTypes.STRING },
    age: {type: DataTypes.BIGINT },
    color: {type: DataTypes.STRING }
})

async function SyncDB() {
    try {
        console.log("vamos a inicializar base datos")

        await Model.sync({logging: false});

        console.log("Base de datos inicializada")

        return { statusCode: 200, data: 'ok'}

    } catch (error) {
        console.log(error);
        return { statusCode: 500, data: error.toSring() }
    }
}

module.exports = { SyncDB, Model }
