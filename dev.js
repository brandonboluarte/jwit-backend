const micro = require('./src');
const { Model } = require('./src/Models');

async function main() {
    try {

        const db = await micro.SyncDB();

        if( db.statusCode !== 200) throw db.message

        micro.run() 

    } catch (error) {
        console.log(error);
    }
}

main()

