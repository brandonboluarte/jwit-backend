const {queueCreate, queueDelete, queueFindOne, queueUpdate, queueView} = require('../src/Adapters/index')

async function Create({age, color, name}) {
    try {
        const job = await queueCreate.add({age, color, name})

        const {statusCode, data, message} = await job.finished()

        if( statusCode===200 ) {
            console.log(data)
        } else {
            console.error(message)
        }


    } catch (error) {
        console.log(error)
    }
}

async function Delete({id}) {
    try {
        const job = await queueDelete.add({id})

        const {statusCode, data, message} = await job.finished()
        if( statusCode===200 ) {
            console.log(data)
        } else {
            console.error(message)
        }

    } catch (error) {
        console.log(error)
    }
}

async function FindOne({ name}) {
    try {
        const job = await queueFindOne.add({name})

        const {statusCode, data, message} = await job.finished()
        if( statusCode===200 ) {
            console.log(data)
        } else {
            console.error(message)
        }

    } catch (error) {
        console.log(error)
    }
}

async function Update({age, color, name, id}) {
    try {
        const job = await queueUpdate.add({age, color, name, id})

        const {statusCode, data, message} = await job.finished()
        
        console.log(statusCode, data, message)

    } catch (error) {
        console.log(error)
    }
}

async function View({}) {
    try {
        const job = await queueView.add({})

        const {statusCode, data, message} = await job.finished()
        if( statusCode===200 ) {
            for(let x of data) console.log(x)
        } else {
            console.error(message)
        }

    } catch (error) {
        console.log(error)
    }
}

async function main() {
    // await Create({name: "Junior", age: 24, color: "rojo"})
    // await Delete({id: 2})
    await Update({name: "Brandon", age: 22, color: "azul", id: 1})
    await View({})
}

main()